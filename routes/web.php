<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{

    DashboardController,
    CompanyController,
    EmploeyController,
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



require __DIR__.'/auth.php';


Route::group([
    'prefix'=>'dashboard',
    'middleware'=>['auth', 'admin'],
    'as'=> 'dashboard.'
    ],function() {
        Route::get('/', [DashboardController::class, 'index'])->name('index');

        Route::resources([
            'companies' => CompanyController::class,
        ]);
        Route::resources([
            'employees' => EmploeyController::class,
        ]);


});
