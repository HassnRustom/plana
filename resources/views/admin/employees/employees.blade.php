@extends('admin.master')
@section('navbar-title', 'الموظفين')

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card my-4">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
          <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 class="text-white text-capitalize pe-3">جدول الموظفين</h6>
          </div>
        </div>
        <div class="card-body px-0 pb-2">
            <div class="button_datatable px-2">
                <a href="{{route('dashboard.employees.create')}}" class="btn btn-success text-sm" id="add_new_user_button" type="button">
                    <i class="fa fa-plus"></i>
                    إضافة موظف جديد
                </a>

            </div>
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0 table-hover " style="width:100%">
                <thead>
                    <tr>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الأسم الكامل</th>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الشركة</th>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الإيميل</th>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7"> الهاتف </th>
                    <th class="text-secondary text-secondary text-s font-weight-bolder opacity-7">الاجراءات</th>
                    </tr>
                </thead>
                @foreach($employees as $emploey)
                    <tr>
                        <td>{{$emploey->first_name}} {{$emploey->last_name}}</td>
                        <td>{{$emploey->company->name}}</td>
                        <td>
                            @if ($emploey->email)
                            {{$emploey->email}}
                            @else
                                لا يوجد
                            @endif
                        </td>
                        <td>
                            @if ($emploey->phone)
                            {{$emploey->phone}}
                            @else
                                لا يوجد
                            @endif
                            </td>
                        <td>
                            {{-- Start For Edite --}}
                            <form method="GET" action="employees/{{ $emploey->id }}/edit" style="display: inline-block">

                                <button type="submit" class="btn btn-success">
                                    تعديل
                                </button>
                            </form>

                            {{-- Start For Delete --}}
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteEmploey{{ $emploey->id }}">
                                حذف
                            </button>
                            <div class="modal fade" id="deleteEmploey{{ $emploey->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title text-center" id="staticBackdropLabel">حذف معلومات الموظف</h5>
                                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" method="POST" action="employees/{{ $emploey->id }}" class="text-end ">
                                            @csrf
                                            @method('delete')
                                            <input type="hidden" name="id" value="{{ $emploey->id }}">
                                            <h4 class="text-black">هل أنت متأكد؟</h4>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">إغلاق</button>
                                      <button type="submit" class="btn btn-primary"> حذف</button>
                                    </div>
                                </form>
                                  </div>
                                </div>
                              </div>
                            {{-- End For Delete --}}
                        </td>
                    </tr>
                @endforeach
                </table>
                {{ $employees->links() }}
            </div>
        </div>
      </div>
    </div>
  </div>

  <!--     toast start     -->
  <div class="position-fixed top-0 start-0 p-lg-3" style="z-index: 11">
    <div class="toast fade hide p-2 mt-2 bg-gradient-info" role="alert" aria-live="assertive" id="infoToast" aria-atomic="true">
        <div class="toast-header bg-transparent border-0">
          <i class="material-icons text-white ms-2">notifications</i>
          <span class="ms-auto text-white font-weight-bold text-lg">اشعار</span>
          <i class="fas fa-times text-md text-white me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
        </div>
        <hr class="horizontal light m-0">
        <div class="toast-body text-white text-lg">
          يتم الآن الحذف ....
        </div>
      </div>

      <div class="toast fade hide p-2 bg-white" role="alert" aria-live="assertive" id="successToast" aria-atomic="true">
        <div class="toast-header border-0">
          <i class="material-icons text-success ms-2">check</i>
          <span class="ms-auto font-weight-bold text-lg">نجاح</span>
          <i class="fas fa-times text-md me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
        </div>
        <hr class="horizontal dark m-0">
        <div class="toast-body text-lg">
         تمت العملية بنجاح
        </div>
      </div>

  </div>
  <!--     toast end     -->

@endsection
