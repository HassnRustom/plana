@extends('admin.master')
@section('navbar-title', 'إضافة شركة')

@section('content')
<div class="row">
    <div class=" col-12">
        <div class="card">
            <div class="card-header pb-0  px-3 ">
                <h6 class="mb-0 text-lg">إضافة شركة جديدة</h6>
            </div>

            <div class="card-body pt-2 px-3">

                <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                    <span class="text-sm"></span>
                    <ul class="text-sm m-0" id="errors_list"></ul>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <script>
                    $("#alert_message").on("close.bs.alert", function () {
                        // hide the alert >>> not remove it
                        $("#alert_message").hide();
                        return false;
                    });
                </script>

                <form enctype="multipart/form-data" method="POST" action="{{ route("dashboard.companies.store") }}" class="text-end ">
                    @csrf

                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1">
                                <label class="form-label">الأسم</label>
                                <input name="name" id="name" type="text"   class="form-control">
                            </div>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1">
                                <label class="form-label">الايميل</label>
                                <input name="email" id="email" value="{{ old('email') }}" type="email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1">
                                <label class="form-label">الموقع</label>
                                <input id="website" name="website" value="{{ old('website') }}" type="text" class="form-control">
                            </div>
                        </div>
                    </div>

                    {{-- <div class="input-group input-group-outline mb-3">
                        <input id="company_image" name="company_image" type="file" class="form-control">
                    </div> --}}
                    <div class="input-group mb-3">
                        <input type="file" class="form-control" name="company_image" accept="image/*" id="inputGroupFile02">
                        <label class="input-group-text" for="inputGroupFile02">Upload</label>
                      </div>

                    <div class="text-center">
                      <button type="submit" class="btn bg-gradient-primary w-50 my-4 mb-2 text-lg">
                        <span id="button_submit_text">إضافة</span>
                      </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<script>
    // Get a reference to the file input element
    const inputElement = document.querySelector('input[id="company_image"]');

    // Create a FilePond instance
    const pond = FilePond.create(inputElement);

    FilePond.setOptions({
    server: {
        url : '/company_image',
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
    }
    });
</script>







@endsection
