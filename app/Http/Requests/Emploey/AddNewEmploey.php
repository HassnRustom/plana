<?php

namespace App\Http\Requests\Emploey;

use Illuminate\Foundation\Http\FormRequest;

class AddNewEmploey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            'first_name'   => 'required',
            'last_name' => 'required',
            'company_id' => 'required',
        ];
    }

    public function messages() {
        return [
            'first_name.required' => 'هذا الحقل مطلوب',
            'last_name.required' => 'هذا الحقل مطلوب',
            'company_id.required' => 'هذا الحقل مطلوب',
        ];
    }
}
