<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\AddNewCompany;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = Company::latest()->paginate(10);
        return view('admin.companies.companies', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.companies.create_company');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewCompany $request)
    {
        //

        $validated = $request->validated();
        $company = new Company();
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        if(!empty($request->company_image)){
            $company->logo = $request->company_image->getClientOriginalName();
        }
        $company->save();
        $destination= 'public/Company'.$company->id;
        if(!empty($request->company_image)){
            $request->company_image->storeAs($destination,$request->company_image->getClientOriginalName());
        }
        return redirect()->route('dashboard.companies.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        $company = Company::findOrFail($request->id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        if(!empty($request->company_image)){
            $destination= 'public/Company'.$request->id.'/'.$company->logo;
            Storage::delete($destination);
            $company->logo = $request->company_image->getClientOriginalName();
        }
        $company->save();
        $destination= 'public/Company'.$company->id;
        if(!empty($request->company_image)){
            $request->company_image->storeAs($destination,$request->company_image->getClientOriginalName());
        }
        return redirect()->route('dashboard.companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $company = Company::findOrFail($request->id);
        $destination= 'public/Company'.$request->id;
        Storage::deleteDirectory($destination);
        $company->delete();
        return redirect()->route('dashboard.companies.index');
    }
}
