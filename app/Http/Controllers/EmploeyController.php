<?php

namespace App\Http\Controllers;

use App\Http\Requests\Emploey\AddNewEmploey;
use App\Models\Company;
use App\Models\Emploey;
use Illuminate\Http\Request;

class EmploeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Emploey::latest()->paginate(10);
        return view('admin.employees.employees', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view('admin.employees.create_emploey', compact('companies'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewEmploey $request)
    {
        //
        $validated = $request->validated();
        Emploey::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_id' => $request->company_id,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);

        return redirect()->route('dashboard.employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Emploey  $emploey
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Emploey  $emploey
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $companies = Company::all();
        $emploey = Emploey::findOrFail($id);
        return view('admin.employees.edite_emploey', compact('companies', 'emploey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Emploey  $emploey
     * @return \Illuminate\Http\Response
     */
    public function update(AddNewEmploey $request)
    {
        //
        $validated = $request->validated();
        Emploey::where('id', $request->id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_id' => $request->company_id,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);

        return redirect()->route('dashboard.employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Emploey  $emploey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $emploey = Emploey::findOrFail($request->id);
        $emploey->delete();
        return redirect()->route('dashboard.employees.index');
    }
}
