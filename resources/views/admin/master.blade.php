<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/admin/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/admin/favicon.png')}}">
  <title>
    @yield('page-title', 'Admin panel')
  </title>


  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{asset('css/admin/material-dashboard.css?v=3.0.0')}}" rel="stylesheet" />

  {{-- custom css --}}
  <link rel="stylesheet" href="{{asset('css/admin/new_style.css')}}">

  <!--   Core JS Files   -->
  <script defer src="{{asset('js/admin/core/popper.min.js')}}"></script>
  <script defer src="{{asset('js/admin/core/bootstrap.min.js')}}"></script>
  <script defer src="{{asset('js/admin/plugins/perfect-scrollbar.min.js')}}"></script>
  <script defer src="{{asset('js/admin/plugins/smooth-scrollbar.min.js')}}"></script>
  <script defer src="{{asset('js/admin/plugins/chartjs.min.js')}}"></script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script defer src="{{asset('js/admin/material-dashboard.js?v=3.0.0')}}"></script>




</head>

<body class="g-sidenav-show rtl bg-gray-200">


  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-end me-3 rotate-caret  bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute start-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="/" >
        <span class="me-1 font-weight-bold text-white">لوحة التحكم</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse px-0 w-auto  max-height-vh-100" >
      <ul class="navbar-nav">

        <li class="nav-item">
          <a class="nav-link text-white {{  request()->routeIs('dashboard.index') ? 'active' : '' }}"  href="{{route('dashboard.index')}}">
            <div class="text-white text-center ms-2 d-flex align-items-center justify-content-center ">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text me-1">الرئيسية</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link text-white {{  request()->routeIs('dashboard.companies.*') ? 'active' : '' }}" href="{{route('dashboard.companies.index')}}">
            <div class="text-white text-center ms-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">apartment</i>
            </div>
            <span class="nav-link-text me-1">الشركات</span>
          </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white {{  request()->routeIs('dashboard.employees.*') ? 'active' : '' }} " href="{{route('dashboard.employees.index')}}">
              <div class="text-white text-center ms-2 d-flex align-items-center justify-content-center ">
                <i class="material-icons opacity-10">people_alt</i>
              </div>
              <span class="nav-link-text me-1">الموظفين</span>
            </a>
          </li>



      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn  mt-4 w-100" href="#" >Hassn</a>
      </div>
    </div>
  </aside>


  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg overflow-x-hidden">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 ">
            <li class="breadcrumb-item text-sm ps-2"><a class="opacity-5 text-dark" href="javascript:;">لوحة القيادة</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">
              @yield('navbar-title')
            </li>
          </ol>
          <h6 class="font-weight-bolder mb-0">
            @yield('navbar-title')
          </h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 px-0" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">أكتب هنا...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav me-auto ms-0 justify-content-end">
            <li class="nav-item d-flex align-items-center d-sm-inline d-none">
              <a href="{{route('logout')}}" class="nav-link text-body font-weight-bold px-0">
                <i class="fas fa-sign-out-alt "></i>
                <span class="d-sm-inline d-none">تسجيل خروج </span>
              </a>
            </li>
            <li class="nav-item d-xl-none pe-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            {{-- <div class="nav-item  pe-3  one-quarter d-flex align-items-center " id="switch_light_dark">
              <input type="checkbox" class="checkbox" id="dark-version" @if(session()->has('dashboard_mode_dark')) checked @endif />
              <label class="label lable-color-change-on-dark" for="dark-version">
                  <i class="fas fa-moon"></i>
                  <i class="fas fa-sun"></i>
                  <div class="ball ball-color-change-on-dark"></div>
              </label>
            </div> --}}


          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->


    <div class="container-fluid py-4">
      <div class="row min-vh-80 h-100">
          <div class="col-12">

            @yield('content')


          </div>
      </div>




      <footer class="footer py-4  ">
        <div class="container-fluid">
          <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
              <!--<div class="copyright text-center text-sm text-muted text-lg-end">-->
              <!--  © <script>-->
              <!--    document.write(new Date().getFullYear())-->
              <!--  </script>,-->
              <!--  made with <i class="fa fa-heart"></i> by-->
              <!--  <a href="https://aracoders.com/" class="font-weight-bold" target="_blank">AraCoders</a>-->
              <!--  for a better web-->
              <!--</div>-->
            </div>
          </div>
        </div>
      </footer>
    </div>
  </main>



</body>

</html>
