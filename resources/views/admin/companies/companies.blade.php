@extends('admin.master')
@section('navbar-title', 'الشركات')

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card my-4">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
          <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 class="text-white text-capitalize pe-3">جدول الشركات</h6>
          </div>
        </div>
        <div class="card-body px-0 pb-2">
            <div class="button_datatable px-2">
                <a href="{{route('dashboard.companies.create')}}" class="btn btn-success text-sm" id="add_new_user_button" type="button">
                    <i class="fa fa-plus"></i>
                    إضافة شركة جديد
                </a>

            </div>
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0 table-hover " style="width:100%">
                <thead>
                    <tr>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الأسم</th>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الإيميل</th>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الموقع الإلكتروني</th>
                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7"> الصورة </th>
                    <th class="text-secondary text-secondary text-s font-weight-bolder opacity-7">الاجراءات</th>
                    </tr>
                </thead>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->name}}</td>
                        <td>
                            @if ($company->email)
                                {{$company->email}}
                            @else
                                لا يوجد
                            @endif

                        </td>
                        <td>
                            @if ($company->website)
                                {{$company->website}}
                            @else
                                لا يوجد
                            @endif
                        </td>
                        <td>
                            @if ($company->logo)
                                <img src="{{ url('storage/'.'Company'.$company->id.'/'.$company->logo) }}" style="width: 75px; height: 75px ; border-radius: 50%; object-fit: cover" alt="alt">
                            @else
                                لا يوجد
                            @endif
                        </td>
                        <td>
                            {{-- Start For Edite --}}
                            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edite{{ $company->id }}">
                                تعديل
                            </button>
                              <!-- Modal -->
                              <div class="modal fade" id="edite{{ $company->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title text-center" id="staticBackdropLabel">تعديل معلومات الشركة</h5>
                                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" method="POST" action="companies/{{ $company->id }}" class="text-end ">
                                            @csrf
                                            @method('PUT')
                                            <input type="hidden" name="id" value="{{ $company->id }}">
                                            <div class="row align-items-center">
                                                <div class="col-12">
                                                    <div class="input-group input-group-outline  is-focused my-1 is-focused">
                                                        <label class="form-label">الأسم</label>
                                                        <input name="name" id="name" type="text" required value="{{ $company->name }}"  class="form-control">
                                                    </div>
                                                    @error('name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col-12">
                                                    <div class="input-group input-group-outline  is-focused my-1">
                                                        <label class="form-label">الايميل</label>
                                                        <input name="email" id="email" value="{{ $company->email }}" type="email" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col-12">
                                                    <div class="input-group input-group-outline  is-focused my-1">
                                                        <label class="form-label">الموقع</label>
                                                        <input id="website" name="website" value="{{ $company->website}}" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center">
                                                @if ($company->logo)
                                                    <img src="{{ url('storage/'.'Company'.$company->id.'/'.$company->logo) }}" style="width: 75px; height: 75px ; border-radius: 50%; object-fit: cover" alt="alt">
                                                @else
                                                    <p class="fw-bold m-3">لا يوجد صورة</p>
                                                @endif
                                            </div>
                                            <div class="input-group mb-3">
                                                <input type="file" class="form-control" name="company_image" accept="image/*" id="inputGroupFile02">
                                                <label class="input-group-text" for="inputGroupFile02">Upload</label>
                                              </div>

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">إغلاق</button>
                                      <button type="submit" class="btn btn-primary">حفظ التعديلات</button>
                                    </div>
                                </form>
                                  </div>
                                </div>
                              </div>
                            {{-- End For Edite --}}
                            {{-- Start For Delete --}}
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#delete{{ $company->id }}">
                                حذف
                            </button>
                            <div class="modal fade" id="delete{{ $company->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title text-center" id="staticBackdropLabel">حذف معلومات الشركة</h5>
                                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" method="POST" action="companies/{{ $company->id }}" class="text-end ">
                                            @csrf
                                            @method('delete')
                                            <input type="hidden" name="id" value="{{ $company->id }}">
                                            <h4 class="text-black">هل أنت متأكد؟</h4>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">إغلاق</button>
                                      <button type="submit" class="btn btn-primary"> حذف</button>
                                    </div>
                                </form>
                                  </div>
                                </div>
                              </div>
                            {{-- End For Delete --}}
                        </td>
                    </tr>
                @endforeach
                </table>
                {{ $companies->links() }}
            </div>
        </div>
      </div>
    </div>
  </div>

  <!--     toast start     -->
  <div class="position-fixed top-0 start-0 p-lg-3" style="z-index: 11">
    <div class="toast fade hide p-2 mt-2 bg-gradient-info" role="alert" aria-live="assertive" id="infoToast" aria-atomic="true">
        <div class="toast-header bg-transparent border-0">
          <i class="material-icons text-white ms-2">notifications</i>
          <span class="ms-auto text-white font-weight-bold text-lg">اشعار</span>
          <i class="fas fa-times text-md text-white me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
        </div>
        <hr class="horizontal light m-0">
        <div class="toast-body text-white text-lg">
          يتم الآن الحذف ....
        </div>
      </div>

      <div class="toast fade hide p-2 bg-white" role="alert" aria-live="assertive" id="successToast" aria-atomic="true">
        <div class="toast-header border-0">
          <i class="material-icons text-success ms-2">check</i>
          <span class="ms-auto font-weight-bold text-lg">نجاح</span>
          <i class="fas fa-times text-md me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
        </div>
        <hr class="horizontal dark m-0">
        <div class="toast-body text-lg">
         تمت العملية بنجاح
        </div>
      </div>

  </div>
  <!--     toast end     -->

@endsection
