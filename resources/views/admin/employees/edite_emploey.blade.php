@extends('admin.master')
@section('navbar-title', 'تعديل موظف')

@section('content')
<div class="row">
    <div class=" col-12">
        <div class="card">
            <div class="card-header pb-0  px-3 ">
                <h6 class="mb-0 text-lg">تعديل معلومات الموظف</h6>
            </div>

            <div class="card-body pt-2 px-3">

                <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                    <span class="text-sm"></span>
                    <ul class="text-sm m-0" id="errors_list"></ul>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form  method="POST" action="/dashboard/employees/{{ $emploey->id }}" class="text-end ">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="id" value="{{ $emploey->id }}">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1 is-focused ">
                                <label class="form-label">الأسم الأول</label>
                                <input name="first_name" value="{{ $emploey->first_name }}" id="first_name" type="text"   class="form-control">
                            </div>
                            @error('first_name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1 is-focused">
                                <label class="form-label">الأسم الأخير</label>
                                <input name="last_name" value="{{ $emploey->last_name }}" id="last_name" type="text"   class="form-control">
                            </div>
                            @error('last_name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1 is-focused">
                                <label class="form-label">الايميل</label>
                                <input name="email" id="email" value="{{ $emploey->email }}" type="email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <select class="form-select form-select-lg mb-3"  name="company_id" aria-label=".form-select-lg example">
                                <option value="">Chose Company</option>
                                @foreach ($companies as $company)
                                    <option
                                        @if ($company->id == $emploey->company_id)
                                            selected
                                        @endif
                                    value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                              </select>
                              @error('company_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1 is-focused">
                                <label class="form-label">رقم الهاتف</label>
                                <input id="phone" name="phone" value="{{ $emploey->phone }}" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn bg-gradient-primary w-50 my-4 mb-2 text-lg">
                        <span id="button_submit_text">تعديل</span>
                      </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
