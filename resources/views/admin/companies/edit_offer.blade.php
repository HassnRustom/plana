@extends('admin.master')
@section('navbar-title', 'تعديل عرض')

@section('content')
<div class="row">
    <div class=" col-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between pb-0 px-3 ">
                <h6 class="mb-0 text-lg">تعديل عرض </h6>
                <img class="" src="/public/storage/offers-image/{{ $offer->image }}" width="100" height="50" style="" alt="">
            </div>

            <div class="card-body pt-4 p-3">


                <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                    <span class="text-sm">حصل خطأ اثناء تعديل بيانات العرض</span>
                    <ul class="text-sm m-0" id="errors_list"></ul>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="display: none"  class="alert alert-success alert-dismissible text-white" role="alert" id="succcess_message">
                    <span class="text-sm">تم تعديل بيانات العرض</span>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <script>
                    $("#alert_message,#succcess_message").on("close.bs.alert", function () {
                        // hide the alert >>> not remove it
                        $("#alert_message").hide();
                        $("#succcess_message").hide();
                        return false;
                    });
                </script>

                <form role="form" id="edit_user_form" class="text-end">
                    <div class="row ">
                        {{-- countries --}}
                        <div class="col-md-6 col-lg-6 my-1">
                            <div class="form-control ">
                                <select
                                class="input-group"
                                id="select-country"
                                name="countries"
                                placeholder="اختر دولة العرض ..."
                                autocomplete="off">
                                    <option  value="">اختر  دولة العرض ...</option>
                                    @foreach ($countries as $item)
                                        <option <?php if($offer->country_id == $item->id ) {echo 'selected';} ?> value="{{ $item->id }}">{{ $item->ar_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- countries --}}
                        {{-- categories --}}
                        <div class="col-md-6 col-lg-6 my-1">
                            <div class="form-control ">
                                <select
                                class="input-group"
                                id="select-category"
                                name="categories"
                                placeholder="اختر تصنيف العرض ..."
                                autocomplete="off">
                                    <option  value="">اختر  تصنيف العرض ...</option>
                                    @foreach ($categories as $item)
                                        <option <?php if($offer->category_id == $item->id ) {echo 'selected';} ?> value="{{ $item->id }}">{{ $item->ar_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- categories --}}
                    </div>



                    <div class="row align-items-center">
                        <div class="col-md-6 col-lg-6">
                            <div class="input-group input-group-outline my-1 is-filled">
                                <label class="form-label">السعر</label>
                                <input id="price" value="{{ $offer->price }}" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-control my-1 ">
                                <select
                                class="input-group"
                                id="currency"
                                name="currency"
                                placeholder="اختر  العملة ..."
                                autocomplete="off">
                                    <option  value="">اختر   العملة ...</option>
                                    <option <?php if($offer->currency == '$' ){echo 'selected';} ?> value="$">$</option>
                                    <option <?php if($offer->currency == '€' ){echo 'selected';} ?> value="€">€</option>
                                    <option <?php if($offer->currency == '£' ){echo 'selected';} ?> value="£">£</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row  align-items-center">
                        <div class="col-md-12 col-lg-4">
                            <div class=" my-3">
                                <div class="form-check ">
                                    <label class="form-check-label">
                                        <input   name="is_tourist_guide" id="is_tourist_guide"
                                        <?php if($offer->is_tourist_guide == 1){echo 'checked';} ?>
                                        type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                        الدليل السياحي
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class=" my-3">
                                <div class="input-group input-group-outline {{ ($offer->is_tourist_guide == 1)? 'is-filled': '' }}">
                                    <label class="form-label">نوع الدليل السياحي بالعربي</label>
                                    <input id="ar_guide_type" type="text" value="{{ $offer->ar_guide_type }}" class="form-control" {{ ($offer->is_tourist_guide == 1)? '': 'disabled' }}>
                                  </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class=" my-3">
                                <div class="input-group input-group-outline {{ ($offer->is_tourist_guide == 1)? 'is-filled': '' }}  ">
                                    <label class="form-label">نوع الدليل السياحي بالأنكليزية</label>
                                    <input id="en_guide_type" type="text" value="{{ $offer->en_guide_type }}" class="form-control" {{ ($offer->is_tourist_guide == 1)? '': 'disabled' }}>
                                  </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class=" my-3">
                                <div class="form-check ">
                                    <label class="form-check-label">
                                        <input   name="is_boat_tour" id="is_boat_tour"
                                        <?php if($offer->is_boat_tour == 1){echo 'checked';} ?>
                                        type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                        رحلة بحرية
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                             <div class="form-group   my-3">
                                <label class="form-label">تفاصيل الرحلات باللغة العربية</label>
                                <textarea class="form-control" id="ar_boat_tour_company"  rows="3">{{ $offer->ar_boat_tour_company }}</textarea>
                            </div>
                            <!--<div class=" my-3">-->
                            <!--    <div class="input-group input-group-outline {{ ($offer->is_boat_tour == 1)? 'is-filled': '' }}">-->
                            <!--        <label class="form-label">شركة الرحلة البحرية</label>-->
                            <!--        <input id="ar_boat_tour_company" value="{{ $offer->ar_boat_tour_company }}" type="text" class="form-control" {{ ($offer->is_boat_tour == 1)? '': 'disabled' }}>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group   my-3">
                                <label class="form-label">تفاصيل الرحلات باللغة الأنكليزية</label>
                                <textarea class="form-control" id="en_boat_tour_company"  rows="3">{{ $offer->en_boat_tour_company }}</textarea>
                            </div>
                            <!--<div class=" my-3">-->
                            <!--    <div class="input-group input-group-outline {{ ($offer->is_boat_tour == 1)? 'is-filled': '' }}">-->
                            <!--        <label class="form-label">شركة الرحلة البحرية</label>-->
                            <!--        <input id="en_boat_tour_company" value="{{ $offer->en_boat_tour_company }}" type="text" class="form-control" {{ ($offer->is_boat_tour == 1)? '': 'disabled' }}>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>

                    
                    {{-- ************************ --}}
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class=" my-3">
                                <div class="form-check ">
                                    <label class="form-check-label">
                                        <input   name="is_corona_check" id="is_corona_check"
                                        <?php if($offer->is_corona_check == 1){echo 'checked';} ?>
                                        type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                        فحص كورونا
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-8">
                            <div class=" my-3">
                                <div class="input-group input-group-outline {{ ($offer->is_corona_check == 1)? 'is-filled': '' }}">
                                    <label class="form-label">عدد الأشخاص بفحص كورونا </label>
                                    <input id="number_of_corona_check" value="{{ $offer->number_of_corona_check }}" type="number" class="form-control" {{ ($offer->is_corona_check == 1)? '': 'disabled' }}>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{-- ************************ --}}




                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-check my-3">
                                <label class="form-check-label">
                                    <input   name="is_transportation" id="is_transportation"
                                    <?php if($offer->is_transportation == 1){echo 'checked';} ?>
                                    type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                    المواصلات
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-check my-3">
                                <label class="form-check-label">
                                    <input   name="is_breakfast" id="is_breakfast"
                                    <?php if($offer->is_breakfast == 1){echo 'checked';} ?>
                                    type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                    بوفيه الإفطار
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-check my-3">
                                <label class="form-check-label">
                                    <input   name="is_airport_reception" id="is_airport_reception"
                                    <?php if($offer->is_airport_reception == 1){echo 'checked';} ?>
                                    type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                    الاستقبال في المطار
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class=" my-3">
                                <div class="form-check ">
                                    <label class="form-check-label">
                                        <input   name="is_description" id="is_description"
                                        <?php if($offer->is_description == 1){echo 'checked';} ?>
                                        type="checkbox" value="1" class="form-check-input" style="margin-left: 5px !important">
                                     خيارات اضافية ضمن العرض
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <span id="myspan1" class="text-danger {{ ($offer->is_description == 1)? '':'visually-hidden' }}">افصل بين الخيارات بعلامة #</span>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group   my-3">
                                <label class="form-label"> الخيارات الاضافية باللغة العربية</label>
                                <textarea class="form-control" id="ar_description" {{ ($offer->is_description == 1)? '': 'disabled' }} rows="3">{{ $offer->ar_description }}</textarea>
                            </div>

                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group   my-3">
                                <label class="form-label">الخيارات الاضافية  باللغة الأنكليزية</label>
                                <textarea class="form-control" id="en_description" {{ ($offer->is_description == 1)? '': 'disabled' }} rows="3">{{ $offer->en_description }}</textarea>
                            </div>

                        </div>
                    </div>
                    
                    <div class="input-group input-group-outline mb-3">
                        <input id="offer_image" name="offer_image" type="file" class="form-control">
                    </div>


                    <!---->
                    
                    
                    
                    <!---->
                    
                    <div class="text-center">
                      <button type="submit" class="btn bg-gradient-primary w-50 my-4 mb-2 text-lg">
                        <span id="button_submit_text">تعديل</span>
                        <div class="spinner" style="display: none;">
                          <div class="double-bounce1"></div>
                          <div class="double-bounce2"></div>
                        </div>
                      </button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</div>




<script>
    new TomSelect("#select-category",{
        create: false,
        persist: false,
    });
</script>
<script>
    new TomSelect("#select-country",{
        create: false,
        persist: false,
    });
</script>
<script>
    new TomSelect("#currency",{
        create: false,
        persist: false,
    });
</script>

<script>
    // Get a reference to the file input element
    const inputElement = document.querySelector('input[id="offer_image"]');

    // Create a FilePond instance
    const pond = FilePond.create(inputElement);

    FilePond.setOptions({
    server: {
        url : '/offer_image',
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
    }
    });
</script>

{{-- ********************************** --}}
<script>
    if ($('input[name="is_corona_check"]:checked').length > 0){
        var disabled2 = false;
    }else{
        var disabled2 = true;
    }

    $('#is_corona_check').click(function() {
        if (disabled2) {
            $("#number_of_corona_check").prop('disabled', false);       // if disabled, enable
        }
        else {
            $("#number_of_corona_check").prop('disabled', true);        // if enabled, disable

            $("#number_of_corona_check").val("");

            $("#number_of_corona_check").parent().removeClass('is-filled');

        }
        disabled2 = !disabled2;
    })
</script>
{{-- ********************************** --}}

<script>
    if ($('input[name="is_tourist_guide"]:checked').length > 0){
        var disabled = false;
    }else{
        var disabled = true;
    }

    $('#is_tourist_guide').click(function() {
        $('#myspan').toggleClass('visually-hidden');
        if (disabled) {
            $("#ar_guide_type").prop('disabled', false);       // if disabled, enable
            $("#en_guide_type").prop('disabled', false);       // if disabled, enable

        }
        else {
            $("#ar_guide_type").prop('disabled', true);        // if enabled, disable
            $("#en_guide_type").prop('disabled', true);        // if enabled, disable
            $("#ar_guide_type").val("");
            $("#en_guide_type").val("");
            $("#ar_guide_type").parent().removeClass('is-filled');
            $("#en_guide_type").parent().removeClass('is-filled');
        }
        disabled = !disabled;
    })
</script>



<script>
    if ($('input[name="is_description"]:checked').length > 0){
        var disabled3 = false;
    }else{
        var disabled3 = true;
    }

    $('#is_description').click(function() {
        $('#myspan1').toggleClass('visually-hidden');
        if (disabled3) {
            $("#ar_description").prop('disabled', false);       // if disabled, enable
            $("#en_description").prop('disabled', false);       // if disabled, enable

        }
        else {
            $("#ar_description").prop('disabled', true);        // if enabled, disable
            $("#en_description").prop('disabled', true);        // if enabled, disable
            $("#ar_description").val("");
            $("#en_description").val("");
            $("#ar_description").parent().removeClass('is-filled');
            $("#en_description").parent().removeClass('is-filled');
        }
        disabled3 = !disabled3;
    })
</script>


<script>
    if ($('input[name="is_boat_tour"]:checked').length > 0){
        var disabled1 = false;
    }else{
        var disabled1 = true;
    }
    $('#is_boat_tour').click(function() {
        if (disabled1) {
            $("#ar_boat_tour_company").prop('disabled', false);       // if disabled, enable
            $("#en_boat_tour_company").prop('disabled', false);       // if disabled, enable
        }
        else {
            $("#ar_boat_tour_company").prop('disabled', true);        // if enabled, disable
            $("#en_boat_tour_company").prop('disabled', true);        // if enabled, disable
            $("#en_boat_tour_company").val("");
            $("#ar_boat_tour_company").val("");
            $("#en_boat_tour_company").parent().removeClass('is-filled');
            $("#ar_boat_tour_company").parent().removeClass('is-filled');
        }
        disabled1 = !disabled1;
    })
</script>


<script>
    $('#edit_user_form').on('submit', function(e) {
       e.preventDefault();


       $("#button_submit_text").hide();
       $(".spinner").show();
       $("#alert_message").hide();
       $("#succcess_message").hide();

       var uploadData = new FormData();

       var country_id = $('#select-country').val();
       var category_id = $('#select-category').val();
       var currency = $('#currency').val();
       var price = $('#price').val();
       var ar_guide_type = $('#ar_guide_type').val();
        var en_guide_type = $('#en_guide_type').val();
       if ($('input[name="is_tourist_guide"]:checked').length > 0 ) {
            var is_tourist_guide = 1;

       }else {
        var is_tourist_guide = 0;
       }
       
       if ($('input[name="is_description"]:checked').length > 0 ) {
            var is_description = 1;

       }else {
        var is_description = 0;
       }
       
       var ar_description = $('#ar_description').val();
        var en_description = $('#en_description').val();
       
        //    ***********************
    if ($('input[name="is_corona_check"]:checked').length > 0 ) {
            var is_corona_check = 1;

       }else {
        var is_corona_check = 0;
       }

       var number_of_corona_check = $('#number_of_corona_check').val();
    //    ***********************
       
       if ($('input[name="is_boat_tour"]:checked').length > 0 ) {
            var is_boat_tour = 1;

       }else {
        var is_boat_tour = 0;
       }
       var ar_boat_tour_company = $('#ar_boat_tour_company').val();
            var en_boat_tour_company = $('#en_boat_tour_company').val();

       if ($('input[name="is_airport_reception"]:checked').length > 0 ) {
           var is_airport_reception = 1 ;
       }else {
        var is_airport_reception = 0 ;
       }
       if ($('input[name="is_breakfast"]:checked').length > 0 ) {
           var is_breakfast = 1;
       }else {
        var is_breakfast = 0;
       }
       if ($('input[name="is_transportation"]:checked').length > 0 ) {
           var is_transportation =1;
       }else {
        var is_transportation =0;
       }

       var image = $("input[name='offer_image']").val();


       if(country_id) {
           uploadData.append('country_id',country_id);
        }
        if(category_id) {
            uploadData.append('category_id',category_id);
        }
        if(currency) {
            uploadData.append('currency',currency);
        }
        if(price) {
            uploadData.append('price',price);
       }
        if(image) {
            uploadData.append('image',image);
       }

            uploadData.append('ar_guide_type',ar_guide_type);


            uploadData.append('en_guide_type',en_guide_type);


            uploadData.append('ar_boat_tour_company',ar_boat_tour_company);


            uploadData.append('en_boat_tour_company',en_boat_tour_company);


            uploadData.append('is_tourist_guide',is_tourist_guide);


            uploadData.append('is_boat_tour',is_boat_tour);


            uploadData.append('is_breakfast',is_breakfast);
            
            uploadData.append('is_description',is_description);
            uploadData.append('ar_description',ar_description);
            uploadData.append('en_description',en_description);


            uploadData.append('is_transportation',is_transportation);


            uploadData.append('is_airport_reception',is_airport_reception);
            
            uploadData.append('is_corona_check',is_corona_check);


            uploadData.append('number_of_corona_check',number_of_corona_check);



       uploadData.append('_method','PUT');


       $.ajax({
           type: "POST",
           headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
           url: '{{route('dashboard.offers.update',$offer)}}',
           data: uploadData,
           contentType: false,
           processData: false,
           statusCode: {
                422: function(error) {
                    // Only if your server returns a 422 status code can it come in this block. :-)
                    var errors = error.responseJSON.errors;
                    // console.log(errors);
                    var alert_message = document.getElementById("alert_message");
                    $("#alert_message").show();
                    var ul = document.getElementById("errors_list");
                    $('#errors_list').empty();
                    Object.entries(errors).forEach(function([key, value]){
                        var li = document.createElement("li");
                        li.appendChild(document.createTextNode(value));
                        ul.appendChild(li);
                    }
                    );
                },
                403: function() {
                    // Only if your server returns a 403 status code can it come in this block. :-)
                }
            },
           success:function(data){
                console.log("success");
                console.log(data);
                $("#button_submit_text").show();
                $(".spinner").hide();
                $("#succcess_message").show();
                window.location = '{{ route("dashboard.offers.index") }}';
           },
           error:function(error){
                console.log("error");
                console.log(error);
                $("#button_submit_text").show();
                $(".spinner").hide();
                $("#alert_message").show();
                // myToast.show();
           }
       });
   });
</script>

@endsection
